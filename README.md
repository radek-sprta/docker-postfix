# postfix/Postfix [![Docker Image Size (latest semver)](https://img.shields.io/docker/image-size/rsprta/postfix)](https://hub.docker.com/r/rsprta/postfix) [![Docker Pulls](https://img.shields.io/docker/pulls/rsprta/postfix)](https://hub.docker.com/r/rsprta/postfix) [![Pipeline status](https://gitlab.com/radek-sprta/docker-postfix/badges/master/pipeline.svg)](https://gitlab.com/radek-sprta/docker-postfix/commits/master)

## Quick reference
- **Maintained by**: [Radek Sprta](https://gitlab.com/radek-sprta)
- **Where to get help**: [Repository Issues](https://gitlab.com/radek-sprta/docker-postfix/-/issues)

## Description
Simple Postfix MSA with SASL authentication and DKIM support.

### Features

- Based on Alpine Linux
- Latest Postfix version
- Support for external DKIM validation
- SMTP authentication
- Customization of all Postfix settings
- Secure TLS settings by default

### Environment variables

| Variable | Description | Type | Default value |
| -------- | ----------- | ---- | ------------- |
| **OPENDKIM_PORT** | Port for DKIM service | *optional* | none
| **OPENDKIM_HOST** | Host for DKIM service | *optional* | none
| **POSTFIX_<variable>** | Customize any postfix <variable> | *optional* | none
| **SMTP_USER** | Authentication user | *optional* | none
| **SMTP_PASSWORD** | Authentication password | *optional* | none

You should overwrite at least `POSTFIX_myhostname` and `POSTFIX_mynetworks`.

## Usage
The simplest way to run the container is the following command:

```bash
docker run --detach -p 587:587 -e POSTFIX_myhostname=example.org -e POSTFIX_mynetworks=10.0.0.0/8 rsprta/postfix
```

Or using `docker-compose.yml`:

```yaml
version: '3'
services:
  postfix:
    container_name: postfix
    image: rsprta/postfix
    restart: unless-stopped
    environment:
      - POSTFIX_myhostname=example.org
      - POSTFIX_mynetworks=10.0.0.0/8
```

## Contact
- [mail@radeksprta.eu](mailto:mail@radeksprta.eu)
- [incoming+radek-sprta/docker-postfix@gitlab.com](incoming+radek-sprta/docker-postfix@gitlab.com)

## License
GNU General Public License v3

## Credits
This package was created with [Cookiecutter][cookiecutter] from [cookiecutter-docker-multiarch](https://gitlab.com/radek-sprta/cookiecutter-docker-multiarch).

[cookiecutter]: https://github.com/audreyr/cookiecutter
