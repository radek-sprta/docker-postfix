#! /bin/bash
set -e

# Configure Postfix:
# POSTFIX_var env -> postconf -e var=$POSTFIX_var
for var in ${!POSTFIX_*}; do
    postconf -e "${var:8}=${!var}"
done

# Configure SASL authentication
if [[ -n "${SMTP_USER}" ]] && [[ -n "${SMTP_PASSWORD}" ]]; then
    echo "Configuring SASL authentication"
    postconf -e smtpd_sasl_auth_enable=yes
    postconf -e broken_sasl_auth_clients=yes
    postconf -e smtpd_sasl_security_options=noanonymous,noplaintext
    postconf -e smtpd_sasl_tls_security_options=noanonymous
    postconf -e smtpd_relay_restrictions=permit_sasl_authenticated,reject_unauth_destination
    echo "${SMTP_PASSWORD}" | saslpasswd2 -p -c -u "$(postconf -h mydomain)" "${SMTP_USER}"
    chown postfix -R /etc/sasl2
    /usr/sbin/saslauthd -a sasldb -c -d &
fi

# Enable port 587
postconf -M submission/inet="submission   inet   n   -   n   -   -   smtpd"

# Configure TLS
if [[ -n "${POSTFIX_smtpd_tls_cert_file}" ]] && [[ -n "${POSTFIX_smtpd_tls_key_file}" ]]; then
    echo "Configuring TLS"
    postconf -P "submission/inet/smtpd_tls_security_level=encrypt"
    postconf -P "submission/inet/milter_macro_daemon_name=ORIGINATING"
fi

# Configure OpenDKIM
if [[ -n "${OPENDKIM_HOST}" ]] && [[ "${OPENDKIM_PORT}" ]]; then
    echo "Configuring OpenDKIM"
    postconf -e milter_protocol=2
    postconf -e milter_default_action=accept
    postconf -e smtpd_milters="inet:${OPENDKIM_HOST}:${OPENDKIM_PORT}"
    postconf -e non_smtpd_milters="inet:${OPENDKIM_HOST}:${OPENDKIM_PORT}"
fi

# Create aliases
postalias /etc/postfix/aliases

# Let's start
/usr/sbin/postfix -c /etc/postfix start-fg &
wait -n
