# syntax=docker/dockerfile:1.4
ARG VERSION=latest
FROM alpine:$VERSION

LABEL maintainer="Radek Sprta <mail@radeksprta.eu>"
LABEL org.opencontainers.image.authors="Radek Sprta <mail@radeksprta.eu>"
LABEL org.opencontainers.image.description="Simple Postfix MSA with SASL authentication and DKIM support."
LABEL org.opencontainers.image.documentation="https://gitlab.com/radek-sprta/docker-postfix/-/blob/master/README.md"
LABEL org.opencontainers.image.licenses="GNU General Public License v3"
LABEL org.opencontainers.image.source="https://gitlab.com/radek-sprta/docker-postfix"
LABEL org.opencontainers.image.title="rsprta/postfix"
LABEL org.opencontainers.image.url="https://gitlab.com/radek-sprta/docker-postfix"

RUN apk --no-cache --upgrade add \
    bash \
    ca-certificates \
    cyrus-sasl \
    cyrus-sasl-crammd5 \
    cyrus-sasl-digestmd5 \
    cyrus-sasl-login \
    cyrus-sasl-ntlm \
    postfix \
    tini \
    && rm -Rf /usr/share/doc && rm -Rf /usr/share/man

# Set up configuration
COPY --link /configs/smtpd.conf  /etc/sasl2/smtpd.conf
COPY --link /scripts/run.sh      /
RUN         chmod +x /run.sh
ENV OPENDKIM_HOST= \
    OPENDKIM_PORT= \
    POSTFIX_maillog_file=/dev/stdout \
    POSTFIX_smtpd_tls_cert_file= \
    POSTFIX_smtpd_tls_key_file= \
    POSTFIX_smtpd_tls_security_level=may \
    POSTFIX_smtpd_tls_ciphers=high \
    POSTFIX_smtpd_tls_exclude_ciphers=aNULL,MD5 \
    POSTFIX_smtpd_tls_protocols=>=TLSv1.2 \
    SMTP_USER= \
    SMTP_PASSWORD=

# Set up volumes
VOLUME     [ "/var/spool/postfix" ]

# Set Postfix run environment
USER       root
WORKDIR    /etc/postfix

HEALTHCHECK --interval=30s --timeout=5s --start-period=10s --retries=3 CMD printf "EHLO healthcheck\n" | nc 127.0.0.1 587 | grep -qE "^220.*ESMTP Postfix"

EXPOSE     587
ENTRYPOINT ["/sbin/tini", "--"]
CMD        ["/run.sh"]
